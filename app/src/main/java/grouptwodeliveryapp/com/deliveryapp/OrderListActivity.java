package grouptwodeliveryapp.com.deliveryapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.Toast;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class OrderListActivity extends AppCompatActivity {
    public static final String DELIVERED_VALUE = "grouptwodeliveryapp.com.DELIVERED";
    public static final String ID_VALUE = "grouptwodeliveryapp.com.ID";
    public static final String POS_VALUE = "grouptwodeliveryapp.com.POSVALUE";
    public static final String FIRST_TIME = "grouptwodeliveryapp.com.FIRSTTIME";

    private LocationManager locationManager;
    private MyLocation locationListener;

    private DBHelper dbHelper;
    private boolean delivered;
    private SharedPreferences sh;

    private Toolbar myToolbar;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        myToolbar = (Toolbar) findViewById(R.id.bar_tool);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        dbHelper = new DBHelper(this);

        Intent intent = getIntent();

        delivered = intent.getBooleanExtra(DELIVERED_VALUE, false);

        sh = getSharedPreferences(SettingsActivity.PREF_KEY, MODE_PRIVATE);
        if(!sh.getBoolean(FIRST_TIME, false)){
            DBHelper dbHelper = new DBHelper(this);
            dbHelper.addRandomOrdersToDatabase();
            SharedPreferences.Editor edit = sh.edit();
            edit.putBoolean(FIRST_TIME, true);
            edit.commit();
        }

        mRecycleView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecycleView.setLayoutManager(mLayoutManager);

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, myToolbar);

        updateListView();

        if(delivered){
            setTitle(R.string.delivered_label_text);
        } else {
            setTitle(R.string.undelivered_label_text);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = MyLocation.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(delivered) {
            getMenuInflater().inflate(R.menu.menu_delivered, menu);
        }else {
            getMenuInflater().inflate(R.menu.menu_undelivered, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.undelivered_menu:
                startOrderListActivity(false);
                return true;
            case R.id.delivered_menu:
                startOrderListActivity(true);
                return true;
            case R.id.settings_menu:
                Intent settings = new Intent(this, SettingsActivity.class);
                settings.putExtra(DELIVERED_VALUE, delivered);
                startActivity(settings);
                return true;
            case R.id.generate_new_orders:
                DBHelper dbHelper = new DBHelper(this);
                dbHelper.addRandomOrdersToDatabase();
                updateListView();
                Toast.makeText(getApplicationContext(), R.string.generated_new_orders_message, Toast.LENGTH_LONG).show();
                return true;
            case R.id.clear_delivered_menu:
                dbHelper = new DBHelper(this);
                dbHelper.clearTableFromDeliveredOrders();
                updateListView();
                return true;
            case R.id.clear_undelivered_menu:
                dbHelper = new DBHelper(this);
                dbHelper.clearTableFromUndeliveredOrders();
                updateListView();
                return true;
            case R.id.exit_menu:
                logOutConfirmation();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startOrderListActivity(boolean delivered){
        Intent undelivered = new Intent(this, OrderListActivity.class);
        undelivered.putExtra(DELIVERED_VALUE, delivered);
        startActivity(undelivered);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!delivered) {
            if(locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, OrderDetailActivity.MIN_UPDATE_TIME,
                        OrderDetailActivity.MIN_UPDATE_DISTANCE, locationListener);
            }
            if(locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER)){
                locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, OrderDetailActivity.MIN_UPDATE_TIME,
                        OrderDetailActivity.MIN_UPDATE_DISTANCE, locationListener);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationManager.removeUpdates(locationListener);
    }

    private void updateListView() {
        Cursor cursor;

        if(delivered) {
            cursor = dbHelper.getDeliveredOrdersFromDatabase();
        } else {
            cursor = dbHelper.getUndeliveredOrdersFromDatabase();
        }

        OrderRecyclerAdapter adapter = new OrderRecyclerAdapter(this, cursor, delivered);

        ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(adapter);
        alphaAdapter.setDuration(400);
        alphaAdapter.setInterpolator(new OvershootInterpolator(2f));
        alphaAdapter.setFirstOnly(false);
        mRecycleView.setAdapter(alphaAdapter);

        adapter.setOnItemClickListener(new OrderRecyclerAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View itemView, int position) {
                Intent intent = new Intent(OrderListActivity.this, OrderDetailActivity.class);
                intent.putExtra(DELIVERED_VALUE, delivered);
                intent.putExtra(POS_VALUE, position);
                startActivity(intent);
            }
        });
    }

    /**
     * Starts delivered activity when delivered link is clicked inside navbar
     * @param view
     */
    public void deliveredNavItemClicked(View view) {
        startOrderListActivity(true);
    }

    /**
     * Starts undelivered activity when undelivered link is clicked inside navbar
     * @param view
     */
    public void notDeleveredNavItemClicked(View view) {
        startOrderListActivity(false);
    }

    /**
     * Starts settings activity when settings link is clicked inside navbar
     * @param view
     */
    public void settingsNavItemClicked(View view) {
        Intent settings = new Intent(this, SettingsActivity.class);
        settings.putExtra(DELIVERED_VALUE, delivered);
        startActivity(settings);
    }

    /**
     * Exits the app when exit link is clicked inside navbar
     * @param view
     */
    public void quitNavItemClicked(View view) {
        logOutConfirmation();
    }

    /**
     * Clears database when clear link is clicked inside navbar
     * @param view
     */
    public void clearListNavItemClicked(View view) {
        dbHelper = new DBHelper(this);
        dbHelper.clearTableFromUndeliveredOrders();
        updateListView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        showLogoutToastMessageAndInvokeFinish();
    }

    private void showLogoutToastMessageAndInvokeFinish() {
        final Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.logging_out_message), Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 2000);
        finish();
    }

    private void logOutConfirmation() {
        AlertDialog.Builder logoutConfirmation  = new AlertDialog.Builder(this);
        logoutConfirmation.setTitle(R.string.logout_title_builder);
        logoutConfirmation.setMessage(R.string.confirmation_logout_title_message);
        logoutConfirmation.setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showLogoutToastMessageAndInvokeFinish();
                dialog.cancel();
            }
        });
        logoutConfirmation.setNegativeButton(R.string.no_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        logoutConfirmation .create().show();
    }
}
