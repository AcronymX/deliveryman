package grouptwodeliveryapp.com.deliveryapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * The RegisterActivity class.
 * This class creates an account for the user an saves the password and username
 * into an external database.
 * Created by Benjamin H 2015-12-15
 */
public class RegisterActivity extends AppCompatActivity {
    private static final String URL = "http://79.170.44.106/benhostingdeveloper.com/database/create_user_account.php";
    private Button registerButton;
    private EditText registerUserName;
    private EditText firstPassWordText;
    private EditText secondPasswordText;
    private RequestQueue requestQueue;
    private StringRequest request;
    private boolean userInputSucceed;
    private String encryptedPw;
    private Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        myToolbar = (Toolbar) findViewById(R.id.bar_tool);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        registerUserName = (EditText) findViewById(R.id.register_user_name_id);
        firstPassWordText = (EditText) findViewById(R.id.register_password_id);
        secondPasswordText = (EditText) findViewById(R.id.register_password_second_id);

        registerButton = (Button) findViewById(R.id.confirmed_button_id);
        requestQueue = Volley.newRequestQueue(this);

        registerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                userInputSucceed = verifyRegisterData(firstPassWordText.getText().toString(), secondPasswordText.getText().toString());

                if(userInputSucceed) {
                    encryptedPw = Cipher.EncryptPassword(firstPassWordText.getText().toString());

                    request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.names().get(0).equals("success")) {
                                    Toast.makeText(getApplicationContext(), jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put(LoginActivity.USER_NAME_KEY, registerUserName.getText().toString());
                            hashMap.put(LoginActivity.PASSWORD_KEY, encryptedPw);

                            return hashMap;
                        }
                    };
                    requestQueue.add(request);
                }
            }
        });
    }

    /**
     * Determine if the variables meet the correct conditions.
     * @param firstPw The first password you entering.
     * @param secondPw The second password you are entering.
     * @return True if the user has typed in correct input, otherwise false.
     */
    private boolean verifyRegisterData(String firstPw, String secondPw) {
        if((registerUserName.getText().toString().isEmpty()) && (secondPw.isEmpty() && (firstPw.isEmpty()))) {
            Toast.makeText(getApplicationContext(), R.string.verify_text_one, Toast.LENGTH_LONG).show();
            return false;
        } else if(registerUserName.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.verify_text_two, Toast.LENGTH_LONG).show();
            return false;
        } else if(firstPw.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.verify_text_three, Toast.LENGTH_LONG).show();
            return false;
        } else if(secondPw.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.verify_text_four, Toast.LENGTH_LONG).show();
            return false;
        } else if(!firstPw.equals(secondPw)) {
            Toast.makeText(getApplicationContext(), R.string.verify_text_five, Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }
}

