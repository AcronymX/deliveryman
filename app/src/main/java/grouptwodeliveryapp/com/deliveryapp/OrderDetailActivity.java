package grouptwodeliveryapp.com.deliveryapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.List;

public class OrderDetailActivity extends AppCompatActivity {
    private LocationManager locationManager;
    private MyLocation locationListener;
    private final int GPS_TIMEOUT = 10000;
    private final int TOAST_LENGTH = Toast.LENGTH_LONG;
    public static final String FROM_ORDER_DETAILS = "grouptwodeliveryapp.com.FROMDETAILS";
    public static final String LATITUDE_ORDER_DETAILS = "grouptwodeliveryapp.com.LATITUDE";
    public static final String LONGITUDE_ORDER_DETAILS = "grouptwodeliveryapp.com.LONGITUDE";
    public static final String MESSAGE_ORDER_DETAILS = "grouptwodeliveryapp.com.MESSAGE";
    public static final int MIN_UPDATE_TIME = 500;
    public static final int MIN_UPDATE_DISTANCE = 0;
    private DBHelper dbHelper;
    private TextView orderNumber;
    private TextView customerNumber;
    private TextView deliveryAddress;
    private TextView orderSum;
    private TextView orderStatus;
    private TextView deliveryDate;
    private TextView deliveryTime;
    private boolean delivered;
    private long id;
    private int cursorListPos;
    private int repeat;
    private double storedLatitude;
    private double storedLongitude;
    private SharedPreferences sh;
    private static final String PREF_KEY = "Prefs";
    private Toolbar myToolbar;
    private boolean ifDelivered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        setUpVariables();

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        showOrderDetails();
    }

    private void setUpVariables() {
        myToolbar = (Toolbar) findViewById(R.id.bar_tool);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = MyLocation.getInstance();

        dbHelper = new DBHelper(this);

        Intent intent = getIntent();

        delivered = intent.getBooleanExtra(OrderListActivity.DELIVERED_VALUE, delivered);
        id = intent.getLongExtra(OrderListActivity.ID_VALUE, id);
        cursorListPos = intent.getIntExtra(OrderListActivity.POS_VALUE, cursorListPos);

        orderNumber = (TextView) findViewById(R.id.order_number);
        customerNumber = (TextView) findViewById(R.id.customer_number);
        deliveryAddress = (TextView) findViewById(R.id.deliver_adress);
        orderSum = (TextView) findViewById(R.id.order_summary);
        orderStatus = (TextView) findViewById(R.id.order_status);
        deliveryDate = (TextView) findViewById(R.id.delivery_date);
        deliveryTime = (TextView) findViewById(R.id.delivery_time);

        repeat = 0;
    }

    private void showOrderDetails() {
        Cursor cursor;

        if(delivered) {

            orderStatus.setText("Levererad!");

            cursor = dbHelper.getDeliveredOrdersFromDatabase();
            cursor.moveToPosition(cursorListPos);

            deliveryDate.setText(cursor.getString(cursor.getColumnIndex(DBHelper.DELIVERY_DATE_KEY)) + ", ");
            deliveryTime.setText(cursor.getString(cursor.getColumnIndex(DBHelper.TIME_STAMP_KEY)));

            storedLatitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.GPS_LATITUDE_KEY)));
            storedLongitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.GPS_LONGITUDE_KEY)));

            View button = findViewById(R.id.delivery_button_id);
            button.setVisibility(View.GONE);

        } else {

            if(repeat < 1) {
                orderStatus.setText(R.string.not_delivered_text_in_detail_activity);
            } else {
                orderStatus.setText(R.string.delivered_text_in_detail_activity);
                deliveryDate.setText(DBHelper.deliveryDate + ", ");
                deliveryTime.setText(DBHelper.timeStamp);
            }

            cursor = dbHelper.getUndeliveredOrdersFromDatabase();
            cursor.moveToPosition(cursorListPos);

        }
        id = Long.parseLong(cursor.getString(cursor.getColumnIndex(DBHelper.ID_KEY)));
        customerNumber.setText(cursor.getString(cursor.getColumnIndex(DBHelper.CUSTOMER_NUMBER_KEY)));
        deliveryAddress.setText(cursor.getString(cursor.getColumnIndex(DBHelper.DELIVERY_ADDRESS_KEY)));
        orderNumber.setText(cursor.getString(cursor.getColumnIndex(DBHelper.ORDER_NUMBER_KEY)));
        orderSum.setText(cursor.getString(cursor.getColumnIndex(DBHelper.ORDER_SUM_KEY)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_complete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startOrderListActivity(delivered);
                return true;
            case R.id.undelivered_menu:
                startOrderListActivity(false);
                return true;
            case R.id.delivered_menu:
                startOrderListActivity(true);
                return true;
            case R.id.settings_menu:
                Intent settings = new Intent(this, SettingsActivity.class);
                settings.putExtra(FROM_ORDER_DETAILS, true);
                startActivity(settings);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startOrderListActivity(boolean delivered){
        Intent intent = new Intent(this, OrderListActivity.class);
        intent.putExtra(OrderListActivity.DELIVERED_VALUE, delivered);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startLocationUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeLocationUpdates();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            switch(requestCode){
                case 1:
                    startLocationUpdates();
                    break;
            }
        }
    }

    /**
     * This returns the latest valid location update, doesn't always work indoor
     * @return a location or null if there is no location
     */
    private Location getLocation(){
        if(!isGPSEnabled()){
            showGPSAlert();
            return null;
        }
        long lastUpdate = locationListener.getLastUpdateTime();
        long currentTime = System.currentTimeMillis();
        // This checks if last location update happened longer ago than the timout time
        if(currentTime-lastUpdate > GPS_TIMEOUT && (!ifDelivered)){
            showToast(R.string.gps_timeout_text);
            return null;
        } else {
            ifDelivered = true;
        }
        if(locationListener.getLocationGPS() == null && locationListener.getLocationNetwork() != null){
            return locationListener.getLocationNetwork();
        }
        return locationListener.getLocationGPS();
    }

    private boolean isGPSEnabled(){
        return locationManager.isProviderEnabled(locationManager.GPS_PROVIDER);
    }

    private void removeLocationUpdates(){
        locationManager.removeUpdates(locationListener);
    }

    private void startLocationUpdates(){
        if(!delivered) {
            if(isGPSEnabled()) {
                locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, MIN_UPDATE_TIME,
                        MIN_UPDATE_DISTANCE, locationListener);
            }
            if(locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER)){
                locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, MIN_UPDATE_TIME,
                        MIN_UPDATE_DISTANCE, locationListener);
            }
        }
    }

    private void showToast(int id){
        Toast.makeText(this, id, TOAST_LENGTH).show();
    }

    private void showToast(String toastText){
        Toast.makeText(this, toastText, TOAST_LENGTH).show();
    }

    private void showGPSAlert(){
        AlertDialog.Builder gpsAlert  = new AlertDialog.Builder(this);
        gpsAlert.setTitle(R.string.gps_alert_title_text);
        gpsAlert.setMessage(R.string.gps_alert_message_text);
        gpsAlert.setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent gpsOtpions = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(gpsOtpions, 1);
                dialog.cancel();
            }
        });
        gpsAlert.setNegativeButton(R.string.no_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        gpsAlert.create().show();
    }

    /**
     * Opens MapsActivity and shows order on map.
     * If undelivered it gets the coordinates from the address.
     * Otherwise it takes the coordinates from the database.
     */
    public void showMapButton(View view) {
        String location;
        double lat = 0;
        double lng = 0;
        Geocoder gc = new Geocoder(this);
        try {

            if(delivered){
                lat = storedLatitude;
                lng = storedLongitude;
                List<Address> list = gc.getFromLocation(lat, lng, 1);
                location = list.get(0).getAddressLine(0);
            }else {
                location = deliveryAddress.getText().toString();
                List<Address> list = gc.getFromLocationName(location, 1);
                Address add = list.get(0);
                lat = add.getLatitude();
                lng = add.getLongitude();
            }

            Intent intent = new Intent(this, MapsActivity.class);
            intent.putExtra(LATITUDE_ORDER_DETAILS, lat);
            intent.putExtra(LONGITUDE_ORDER_DETAILS, lng);
            intent.putExtra(MESSAGE_ORDER_DETAILS, location);
            startActivity(intent);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes a specific order from database.
     * Saves the specific order to the table DeliveredOrders and then delete it from
     * the UndeliveredOrders table.
     * @param view
     */
    public void deliverOrderButton(View view) {
        //This location might be null if there is no gps location
        Location location = getLocation();

        if(location != null) {

            if(repeat < 1) {

                dbHelper.saveUndeliveredOrderToDeliveredOrders(id, location);
                repeat++;

                showOrderDetails();
                showToast(R.string.order_delivered_text);

                dbHelper.deleteUndeliveredOrderFromDataBase(id);

                removeLocationUpdates();
                sendSms();

            } else {
                showToast(R.string.order_already_delivered);
            }
        }
    }

    /**
     * Gets number from shared preference and sends SMS.
     */
    public void sendSms() {
        sh = getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        String phoneNumber = sh.getString("phoneNumKey", null);

        String textMessage = getString(R.string.sms_order_text) + orderNumber.getText().toString() + getString(R.string.sms_is_delivered_text);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber, null, textMessage, null, null);
            showToast(getString(R.string.sms_sent_to_text) + phoneNumber);
        }catch (Exception e) {
            showToast(getString(R.string.delivery_sms_fail_text));
        }
    }
}
