package grouptwodeliveryapp.com.deliveryapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by TeamDeliverMan on 15-11-18.
 */
public class OrderCursorAdapter extends CursorAdapter {
    private boolean delivered;

    public OrderCursorAdapter(Context context, Cursor c, boolean delivered) {
        super(context, c, 0);
        this.delivered = delivered;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if(delivered){
            return layoutInflater.inflate(R.layout.delivered_order_list, null);
        }else{
            return layoutInflater.inflate(R.layout.undelivered_order_list, null);
        }
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if(delivered){
            TextView orderNumberFromDatabaseTextView = (TextView) view.findViewById(R.id.ordernr_from_database);
            TextView deliveryDateFromDatabaseTextView = (TextView) view.findViewById(R.id.delivery_date_from_database);
            TextView timeStampFromDatabaseTextView = (TextView) view.findViewById(R.id.delivery_timestamp_from_database);

            String orderNumber = cursor.getString(cursor.getColumnIndex(DBHelper.ORDER_NUMBER_KEY));
            String deliveryDate = cursor.getString(cursor.getColumnIndex(DBHelper.DELIVERY_DATE_KEY));
            String timeStamp = cursor.getString(cursor.getColumnIndex(DBHelper.TIME_STAMP_KEY));

            orderNumberFromDatabaseTextView.setText(orderNumber);
            deliveryDateFromDatabaseTextView.setText(deliveryDate);
            timeStampFromDatabaseTextView.setText(timeStamp);
        }else {
            TextView addressFromDatabaseTextView = (TextView) view.findViewById(R.id.address_from_database);
            TextView customerNumberFromDatabaseTextView = (TextView) view.findViewById(R.id.customer_number_from_database);
            TextView orderNumberFromDatabaseTextView = (TextView) view.findViewById(R.id.order_number_from_database);
            TextView incomingOrderDateLabelTextView = (TextView) view.findViewById(R.id.incoming_order_date_label);

            String address = cursor.getString(cursor.getColumnIndex(DBHelper.DELIVERY_ADDRESS_KEY));
            String customerNumber = cursor.getString(cursor.getColumnIndex(DBHelper.CUSTOMER_NUMBER_KEY));
            String orderNumber = cursor.getString(cursor.getColumnIndex(DBHelper.ORDER_NUMBER_KEY));
            String incomingOrderDate = cursor.getString(cursor.getColumnIndex(DBHelper.INCOMING_ORDER_DATE_KEY));

            addressFromDatabaseTextView.setText(address);
            customerNumberFromDatabaseTextView.setText(customerNumber);
            orderNumberFromDatabaseTextView.setText(orderNumber);
            incomingOrderDateLabelTextView.setText(incomingOrderDate);
        }

    }

}
