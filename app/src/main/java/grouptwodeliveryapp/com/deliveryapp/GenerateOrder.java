package grouptwodeliveryapp.com.deliveryapp;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Random;

/**
 * Allows the user to randomly set four variables and get those.
 * The variables are not set when first creating an instance, calling newOrder() is required.
 * Created by TeamDeliverMan on 15-11-19.
 */
public class GenerateOrder {
    private String address;
    private String ordernr;
    private String costumernr;
    private String sum;
    private Context context;
    public static final String ORDER_NUMBER = "grouptwodeliveryapp.com.ORDERNUMBER";

    /**
     * The cunstructor, does not set the variables.
     * @param context the context of the activity using it.
     */
    public GenerateOrder(Context context){
        this.context = context;
    }

    /**
     * Generates new values to for an order.
     */
    public void newOrder(){
        Random rand = new Random();
        newAdress(rand);
        newOrdernr();
        newCostumernr(rand);
        newSum(rand);
    }

    private void newAdress(Random rand){
        ArrayList<String> allAdresses = new ArrayList<>();
        allAdresses.add("Mölndalsvägen 77");
        allAdresses.add("Viktor Rydbergsgatan 14");
        allAdresses.add("Kärravägen 83");
        allAdresses.add("Andra Långgatan 20");
        allAdresses.add("Göteborgsvägen 31");
        int temp = rand.nextInt(allAdresses.size());
        address = allAdresses.get(temp);
    }

    private void newOrdernr(){
        SharedPreferences storedValue = context.getSharedPreferences(ORDER_NUMBER, context.MODE_PRIVATE);
        int temp = storedValue.getInt(ORDER_NUMBER, 0) + 1;
        ordernr = "000000";
        if(temp > 999999){
            temp = 0;
        }else {
            StringBuilder builder = new StringBuilder(ordernr);
            String tempString = String.valueOf(temp);

            int length = 5; //Max charAt value ordernr can have
            //gives i the max charAt value of tempString then loops until the lowest chatAt
            for(int i=tempString.length()-1;i>=0;i--){
                //replaces the string at length with the char at i in tempstring
                builder.setCharAt(length, tempString.charAt(i));
                length--;
            }

            ordernr = builder.toString();
        }
        SharedPreferences.Editor editor = storedValue.edit();
        editor.putInt(ORDER_NUMBER, temp);
        editor.commit();
    }

    private void newCostumernr(Random rand){
        ArrayList<String> allNumbers = new ArrayList<>();
        allNumbers.add("800226");
        allNumbers.add("990002");
        allNumbers.add("509400");
        allNumbers.add("206022");
        allNumbers.add("557700");
        int temp = rand.nextInt(allNumbers.size());
        costumernr = allNumbers.get(temp);
    }

    private void newSum(Random rand){
        int temp = rand.nextInt(20000) + 1;
        sum = String.valueOf(temp) + " kr";
    }

    /**
     * @return a String representing an address
     */
    public String getAddress(){
        return address;
    }

    /**
     * @return a String representing an order number
     */
    public String getOrdernr(){
        return ordernr;
    }

    /**
     * @return a String representing a costumer number
     */
    public String getCostumernr(){
        return costumernr;
    }

    /**
     * @return a String representing an order sum
     */
    public String getSum(){
        return sum;
    }

}
