package grouptwodeliveryapp.com.deliveryapp;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {
    private View containerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    //Variables for drawer remember drawer position
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;

    //String for saving and getting saved preference of drawer position. Name of preference file and key.
    public static final String PREF_FILE_NAME="deliveryPref";
    public static final String KEY_USER_REMEMBERED_DRAWER="user_remembered_drawer";

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Checks if drawer has been started before and save value from readFromPreference to boolean.
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_REMEMBERED_DRAWER, "false"));

        if(savedInstanceState != null) {
            mFromSavedInstanceState = true;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

    }


    /**
     * Sets up the drawer with a toggle on the toolbar by attaching a DrawListener. Shows and hides the drawer.
     * Drawer is closed when app starts.
     * @param fragmentId
     * @param drawerLayout
     * @param toolbar
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {

        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                //If drawer not been opened before set mUserLearned to true and save state to preference.
                if(!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_REMEMBERED_DRAWER, mUserLearnedDrawer + "");
                }
                //Redraw the Actionbar after that drawer hides it upon opening.
                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                getActivity().invalidateOptionsMenu();
            }

            //Called when a drawer's position changes. Changes Alpha value on toolbar accordingly to drawers position.
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
                Log.d("Deliveryman", "offset " +slideOffset);

                if (slideOffset < 0.7) {
                    toolbar.setAlpha(1 - slideOffset);
                }
            }
        };

        //If Drawer is not open and has never been opened open drawer.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(containerView);
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //Syncronize the state of the drawer indicator/affordance with the linked DrawerLayout.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    /**
     * Saving drawer state using apply() to SharedPreference.
     * @param context
     * @param preferenceName
     * @param prefenrenceValue
     */
    public static void saveToPreferences(Context context, String preferenceName, String prefenrenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(preferenceName, prefenrenceValue);
        editor.apply();
    }

    /**
     * Reading sharedPreferences.
     * @param context
     * @param preferenceName
     * @param defaultValue
     * @return
     */
    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);

        return sharedPreferences.getString(preferenceName, defaultValue);
    }
}
