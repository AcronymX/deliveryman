package grouptwodeliveryapp.com.deliveryapp;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * A simple location listener for using the gps.
 * Created by TeamDeliveryMan on 15-11-26.
 */
public class MyLocation implements LocationListener {
    private Location locationGPS = null;
    private Location locationNetwork = null;
    private long lastUpdate = 0;
    private static MyLocation instance = null;

    private MyLocation(){}

    /**
     * Either creates a new instance of MyLocation or returns one if there already is one.
     * @return an instance of MyLocation.
     */
    public static MyLocation getInstance(){
        if(instance == null){
            instance = new MyLocation();
        }
        return instance;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location.getProvider().equals(LocationManager.GPS_PROVIDER)){
            locationGPS = location;
        } else if(location.getProvider().equals(LocationManager.NETWORK_PROVIDER)){
            locationNetwork = location;
        }
        lastUpdate = System.currentTimeMillis();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * The location of the latest gps change in location.
     * @return a location.
     */
    public Location getLocationGPS(){
        return locationGPS;
    }

    /**
     * The location of the latest network change in location.
     * @return a location.
     */
    public Location getLocationNetwork(){
        return locationNetwork;
    }

    /**
     * The time in milli seconds of the latest location change.
     * @return a long.
     */
    public long getLastUpdateTime(){
        return lastUpdate;
    }
}
