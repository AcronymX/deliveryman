package grouptwodeliveryapp.com.deliveryapp;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * The LoginActivity class.
 * Logging in the user.
 * Created by Benjamin H 2015-12-15
 */
public class LoginActivity extends AppCompatActivity {
    private static final String URL = "http://79.170.44.106/benhostingdeveloper.com/database/login.php";
    private LocationManager locationManager;
    private MyLocation locationListener;
    private EditText userName;
    private EditText password;
    private Button logInButton;
    private RequestQueue requestQueue;
    private StringRequest request;
    private String encryptedPw;
    private Toolbar myToolbar;

    public static final String USER_NAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupGPS();

        myToolbar = (Toolbar) findViewById(R.id.bar_tool);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        userName = (EditText) findViewById(R.id.user_name_id);
        password = (EditText) findViewById(R.id.password_id);
        logInButton = (Button) findViewById(R.id.login_button_id);

        requestQueue = Volley.newRequestQueue(this);

        logInButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                encryptedPw = Cipher.EncryptPassword(password.getText().toString());

                request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.names().get(0).equals(getString(R.string.sucess_text))) {
                                Toast.makeText(getApplicationContext(), jsonObject.getString(getString(R.string.sucess_text)),
                                        Toast.LENGTH_LONG).show();

                                startActivity(new Intent(LoginActivity.this, OrderListActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString(getString(R.string.error_text)),
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put(USER_NAME_KEY, userName.getText().toString());
                        hashMap.put(PASSWORD_KEY, encryptedPw);

                        return hashMap;
                    }
                };
                requestQueue.add(request);
            }
        });
    }

    /**
     * Starting RegisterActivity when the register button is pressed.
     * @param view
     */
    public void startRegisterActivity(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    private void setupGPS(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = MyLocation.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, OrderDetailActivity.MIN_UPDATE_TIME,
                    OrderDetailActivity.MIN_UPDATE_DISTANCE, locationListener);
        }
        if(locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER)){
            locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, OrderDetailActivity.MIN_UPDATE_TIME,
                    OrderDetailActivity.MIN_UPDATE_DISTANCE, locationListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                System.exit(0);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}