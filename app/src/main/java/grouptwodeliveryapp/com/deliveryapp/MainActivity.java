package grouptwodeliveryapp.com.deliveryapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView icon = (ImageView) findViewById(R.id.iconBrand);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.alpha_animation);
        icon.startAnimation(animation);

        new Handler().postDelayed(new Runnable() {
            /*
             *Shows Splash screen MainActivity with a timer of
             * SPLASH_TIME_OUT
             */

            @Override
            public void run() {
                //Method is executed after timer is finished.
                //Starts OrderListActivity
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);

                //Close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
