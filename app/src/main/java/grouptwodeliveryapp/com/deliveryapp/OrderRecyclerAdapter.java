package grouptwodeliveryapp.com.deliveryapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.ViewHolder> {
    CursorAdapter mCursorAdapter;
    Context mContext;
    final boolean delivered;

    /**
     * Encapsulates the CursorAdapter and binds its data to a new view for displaying the data in a RecycleVeiw.
     * Calls on onBindViewHolder to bind View to the RecycleView.ViewHolder
     * contents with the item at the given position. Sets up some variabler to be used by the RecyclerView.
     * @param context
     * @param c
     * @param delivered
     */
    public OrderRecyclerAdapter(Context context, Cursor c, final boolean delivered) {

        this.delivered = delivered;
        mContext = context;

        mCursorAdapter = new CursorAdapter(mContext, c, 0) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {

                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

                if(delivered) {
                    return layoutInflater.inflate(R.layout.delivered_order_list, null);
                } else {
                    return layoutInflater.inflate(R.layout.undelivered_order_list, null);
                }
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {

                if(delivered){
                    TextView orderNumberFromDatabaseTextView = (TextView) view.findViewById(R.id.ordernr_from_database);
                    TextView deliveryDateFromDatabaseTextView = (TextView) view.findViewById(R.id.delivery_date_from_database);
                    TextView timeStampFromDatabaseTextView = (TextView) view.findViewById(R.id.delivery_timestamp_from_database);

                    String orderNumber = cursor.getString(cursor.getColumnIndex(DBHelper.ORDER_NUMBER_KEY));
                    String deliveryDate = cursor.getString(cursor.getColumnIndex(DBHelper.DELIVERY_DATE_KEY));
                    String timeStamp = cursor.getString(cursor.getColumnIndex(DBHelper.TIME_STAMP_KEY));

                    orderNumberFromDatabaseTextView.setText(orderNumber);
                    deliveryDateFromDatabaseTextView.setText(deliveryDate);
                    timeStampFromDatabaseTextView.setText(timeStamp);
                } else {
                    TextView addressFromDatabaseTextView = (TextView) view.findViewById(R.id.address_from_database);
                    TextView customerNumberFromDatabaseTextView = (TextView) view.findViewById(R.id.customer_number_from_database);
                    TextView orderNumberFromDatabaseTextView = (TextView) view.findViewById(R.id.order_number_from_database);
                    TextView incomingOrderDateLabelTextView = (TextView) view.findViewById(R.id.incoming_order_date_label);

                    String address = cursor.getString(cursor.getColumnIndex(DBHelper.DELIVERY_ADDRESS_KEY));
                    String customerNumber = cursor.getString(cursor.getColumnIndex(DBHelper.CUSTOMER_NUMBER_KEY));
                    String orderNumber = cursor.getString(cursor.getColumnIndex(DBHelper.ORDER_NUMBER_KEY));
                    String incomingOrderDate = cursor.getString(cursor.getColumnIndex(DBHelper.INCOMING_ORDER_DATE_KEY));

                    addressFromDatabaseTextView.setText(address);
                    customerNumberFromDatabaseTextView.setText(customerNumber);
                    orderNumberFromDatabaseTextView.setText(orderNumber);
                    incomingOrderDateLabelTextView.setText(incomingOrderDate);
                }
            }
        };
    }

    // Define listener member variable
    private static OnItemClickListener listener;

    /**
     * Defines the listener interface
     */
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    /**
     * Define the method that allows the parent activity or fragment to define the listener
     * @param listener
     */
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    /**
     * Used to cache the views within the item layout for fast access.
     * Extends RecycleView.ViewHolder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * ViewHolder holds reference to UI components for each row in RecycleView.
         * Attaches a setOnClickListener on each rendered ViewHolder.
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);

            this.itemView.findViewById(R.id.my_recycler_view);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int itemPosition = getAdapterPosition();
                    if (listener != null)
                        listener.onItemClick(ViewHolder.this.itemView, itemPosition);
                    RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.cell_layout);
                    rl.setBackgroundResource(R.drawable.background_hover);
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Passing the inflater job to the cursor-adapter
        View v = mCursorAdapter.newView(mContext, mCursorAdapter.getCursor(), parent);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Passing the binding operation to cursor loader
        mCursorAdapter.getCursor().moveToPosition(position);
        mCursorAdapter.bindView(holder.itemView, mContext, mCursorAdapter.getCursor());
    }

    @Override
    public int getItemCount() {
        return mCursorAdapter.getCount();
    }
}
