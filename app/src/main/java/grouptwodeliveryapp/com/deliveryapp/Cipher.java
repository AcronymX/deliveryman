package grouptwodeliveryapp.com.deliveryapp;

/**
 * Encryption class
 * Created by Benjamin H 2015-12-15
 */
public class Cipher {

    /**
     * Encrypts a password.
     * Encrypts the unencrypted password you are entering when you are logging in
     * or creating a new account using a simple algorithm.
     * @param unencryptedPw The unencrypted password.
     * @return The password in encrypted format.
     */
    public static String EncryptPassword(String unencryptedPw) {
        char ch;
        String temp = "";

        int key = (unencryptedPw.length() * 2);

        for (int i = 0; i <= unencryptedPw.length() - 1; i++) {
            ch = unencryptedPw.charAt(i);
            ch = (char)(ch + key);
            temp += ch;
        }

        return temp;
    }
}
